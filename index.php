<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title>Tracker activity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <style>
        .myButton {
            box-shadow:inset 0px 1px 0px 0px #bbdaf7;
            background:linear-gradient(to bottom, #79bbff 5%, #378de5 100%);
            background-color:#79bbff;
            border-radius:6px;
            border:1px solid #84bbf3;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:Arial;
            font-size:15px;
            font-weight:bold;
            padding:6px 10px;
            text-decoration:none;
            text-shadow:0px 1px 0px #528ecc;
        }
        .myButton:hover {
            background:linear-gradient(to bottom, #378de5 5%, #79bbff 100%);
            background-color:#378de5;
        }
        .myButton:active {
            position:relative;
            top:1px;
        }
    </style>
</head>
<body>

<table
    align="center"
    rules="rows"
    style="width:60%;">
    <tr>
        <td>

            <table
                background="images/168.png"
                bgcolor="#B0E2FF"
                cellpadding="10"
                style="width:100%; border-radius:5px;">
                <tr>
                    <th>
                        <h1 align="center">Activity tracker</h1>
                    </th>
                </tr>
            </table>
            <br>

            <table
                    background="images/168.png"
                    bgcolor="#D1EEEE"
                    cellpadding="5"
                    style="width:100%; border-radius:5px;">

                <tr>
                    <td>
                        <h5>Add new activity:</h5>
                        <button class="myButton" id="control" onclick="changeState();">Start</button>
                        <button class="myButton" id="reset" onclick="reset();">RESET</button>
                        <script type="text/javascript" src="timer.js"></script>
                    </td>
                    <form action="" method="post">
                    <td>
                        <input id="my_timer" value="00:00:00" class="form-control form-control-sm" type="text" placeholder="Start time" size="8" name="StartTime">
                    </td>
                    <td>
                        <input id="finish" class="form-control form-control-sm" type="text" placeholder="Finish time" size="8" name="FinishTime">
                    </td>
                    <td>
                        <input id="speed" class="form-control form-control-sm" type="text" placeholder="Speed user" size="8" name="speed">
                    </td>
                    <td>
                        <input id="distance" class="form-control form-control-sm" type="text" placeholder="Distance" size="8" name="distance">
                    </td>
                    <td>
                            <select class="form-control form-control-sm" name="type">
                                <option value="0">Select activity type:</option>
                                <option value="Run">Run</option>
                                <option value="Ride">Ride</option>
                            </select>
                    </td>
                    <td>
                        <div class="container">
                            <button class="btn btn-secondary" type="submit">Save</button>
                        </div>
                    </td>
                    </form>
                </tr>
            </table>

            <br />

            <table class="table table-striped" bgcolor="#FFFFE0">
                <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Activity</th>
                    <th scope="col">Distance</th>
                    <th scope="col">Hour</th>
                    <th scope="col">Km / hour</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php $date = date("F d"); echo $date; ?></td>
                    <td><?php echo $_POST['type']; ?></td>
                    <td><?php echo $_POST['distance'] . " km"; ?></td>
                    <td><?php echo $_POST['FinishTime']; ?></td>
                    <td><?php echo $_POST['speed']; ?></td>
                </tr>
                <tr>
                    <?php
                    $connect = mysqli_connect('127.0.0.1','root','','tracker');
                    $query = mysqli_query($connect,"INSERT INTO data_activity
                    (Start_time, Finish_time, Speed, Distance, Activity_type)
                    VALUES ('$_POST[StartTime]', '$_POST[FinishTime]', '$_POST[speed]', '$_POST[distance]', '$_POST[type]')");
                    ?>
                </tr>
                </tbody>
            </table>
            <table class="table table-hover" bgcolor="#E6E6FA">
                <thead>
                <tr>
                    <th>Longest ride</th>
                    <th></th>
                    <th>Longest run</th>
                    <th></th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Date</td>
                    <td><?php $date = date("F d"); echo $date; ?></td>
                    <td>Date</td>
                    <td><?php $date = date("F d"); echo $date; ?></td>
                    <td rowspan="3">
                        <strong><h6>Total ride distance:
                           <?php
                                if ($_POST['distance'] > 15 && $_POST['type'] === 'Ride')
                                {
                                    echo $_POST['distance'] . " km";
                                }
                           ?>
                            </h6><br>
                            <h6>Total run distance:
                              <?php
                                if ($_POST['distance'] > 5 && $_POST['type'] === 'Run')
                                {
                                    echo $_POST['distance'] . " km";
                                }
                              ?>
                            </h6>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td>Distance</td>
                    <td>
                        <?php
                        if ($_POST['distance'] > 15 && $_POST['type'] === 'Ride')
                        {
                            echo $_POST['distance'] . " km";
                        }
                        ?>
                    </td>
                    <td>Distance</td>
                    <td>
                        <?php
                            if ($_POST['distance'] > 5 && $_POST['type'] === 'Run')
                            {
                                echo $_POST['distance'] . " km";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Hour</td>
                    <td>
                        <?php
                            if ($_POST['type'] === $_POST['value'] = 'Ride')
                            {
                                echo $_POST['FinishTime'];
                            }
                        ?>
                    </td>
                    <td>Hour</td>
                    <td>
                        <?php
                            if ($_POST['type'] === $_POST['value'] = 'Run')
                            {
                                echo $_POST['FinishTime'];
                            }
                        ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
